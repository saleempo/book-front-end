import { createApp } from 'vue'
import App from './App.vue'
import * as VueRouter from 'vue-router'

const routes = [
  { 
    path: '/',
    name: 'home-page',
    component: () => import("@/views/Home.vue"),
  },
  { 
    path: '/:uid',
    name: 'details-page',
    component: () => import("@/views/DetailsPage.vue")
  },
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = VueRouter.createRouter({
  routes, // short for `routes: routes`
  //mode: 'history',
  history: VueRouter.createWebHistory(),
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
// const router = VueRouter.createRouter({
//   // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
//   history: VueRouter.createWebHashHistory(),
//   routes, // short for `routes: routes`
// })

const app = createApp(App)
app.use(router)
app.mount('#app')


// const routes = [
//   { 
//     path: '/',
//     name: 'home-page',
//     component: () => import("@/components/Home.vue"),
//   },
//   // { path: '/setup',
//   //   name: 'setup-page',
//   //   component: () => import("@/components/Setup.vue")
//   // },
//   { 
//     path: '/:uid',
//     name: 'details-page',
//     component: () => import("@/components/DetailsPage.vue")
//   },
//   // { 
//   //   path: '/:shop_id',
//   //   name: 'shop-setup-page',
//   //   component: () => import("@/components/shop/ShopSetup.vue")
//   // },
//   { path: '/privacy',
//     name: 'privacy-page',
//     component: () => import("@/components/Orders1.vue")
//   }
// ]

// // 3. Create the router instance and pass the `routes` option
// // You can pass in additional options here, but let's
// // keep it simple for now.
// const router = new VueRouter({
//   routes, // short for `routes: routes`
//   mode: 'history',
//   scrollBehavior (to, from, savedPosition) {
//     if (savedPosition) {
//       return savedPosition
//     } else {
//       return { x: 0, y: 0 }
//     }
//   }
// })

// Vue.config.productionTip = false

// new Vue({
//   router,
//   render: h => h(App)
// }).$mount('#app')



// // 1. Define route components.
// // These can be imported from other files
// const Home = { template: '<div>Home</div>' }
// const About = { template: '<div>About</div>' }

// // 2. Define some routes
// // Each route should map to a component.
// // We'll talk about nested routes later.
// const routes = [
//   { path: '/', component: Home },
//   { path: '/about', component: About },
// ]

// // 3. Create the router instance and pass the `routes` option
// // You can pass in additional options here, but let's
// // keep it simple for now.
// const router = VueRouter.createRouter({
//   // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
//   history: VueRouter.createWebHashHistory(),
//   routes, // short for `routes: routes`
// })

// // 5. Create and mount the root instance.
// const app = Vue.createApp({})
// // Make sure to _use_ the router instance to make the
// // whole app router-aware.
// app.use(router)

// app.mount('#app')

// // Now the app has started!